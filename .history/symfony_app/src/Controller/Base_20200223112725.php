<?php
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
/*----------------------------------------------*
 *this page we will display how to work class php or php controller with template*
 *or other terme how render a template or page html in symfony*
 *----------------------------------------------*/
//^^
 namespace App\Controller;
 use Symfony\Component\HttpFoundation\Response;
 #we use Abstract class to give us possibility to use twig
 use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
 use Symfony\Component\Routing\Annotation\Route;
 class Base extends AbstractController
 {
     public function display()
     {
         return new Response('my name is choaib');
     }
     public function displaytemplate()
     {
         #render function took 2 parametres first the template second the variable
         #who we need to display in the template
         return $this->render('display/display.html.twig',['resultat'=>"hello naimy"]);
     }

     public function displaywithvariable()
     {
            return new response($slug);
     }
 }
?>